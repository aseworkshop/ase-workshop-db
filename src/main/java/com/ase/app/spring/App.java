package com.ase.app.spring;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.ase.app.spring.model.Client;
import com.ase.app.spring.model.Ticket;
import com.ase.app.spring.service.TicketService;

public class App 
{
	private static TicketService ticketService;
	private static ApplicationContext context;
	 
    public static void main( String[] args )
    {
    	context = new ClassPathXmlApplicationContext(
				"applicationContext.xml");
    	
    	ticketService = (TicketService) context.getBean(TicketService.class);
		
    	Calendar calendar = Calendar.getInstance();
    	calendar.set(2016, 8, 10);
    	
    	Client client = new Client("Andrei", "Costel", "RO");
    	
    	Ticket ticket = ticketService.reserve(client, calendar.getTime());
    	
    	Collection<BigDecimal> prices = ticketService.getPrices(new Date(), new Date());
        System.out.println( ticket);
        System.out.println(prices);
    }
}
