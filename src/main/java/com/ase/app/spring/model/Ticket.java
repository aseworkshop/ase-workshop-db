package com.ase.app.spring.model;

import java.math.BigDecimal;
import java.util.Date;

public class Ticket {
	
	private Client client;
	private Date flightDate;
	private BigDecimal price;
	
	public Client getClient() {
		return client;
	}
	public void setClient(Client client) {
		this.client = client;
	}
	public Date getFlightDate() {
		return flightDate;
	}
	public void setFlightDate(Date flightDate) {
		this.flightDate = flightDate;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	@Override
	public String toString() {
		return "Ticket [client=" + client + ", flightDate=" + flightDate + ", price=" + price + "]";
	}
	
	
}
