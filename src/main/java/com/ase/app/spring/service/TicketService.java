package com.ase.app.spring.service;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;

import com.ase.app.spring.model.Client;
import com.ase.app.spring.model.Ticket;

public interface TicketService {
	Ticket reserve(Client client, Date flightDate);
	Collection<BigDecimal> getPrices(Date startDate, Date endDate);
}
