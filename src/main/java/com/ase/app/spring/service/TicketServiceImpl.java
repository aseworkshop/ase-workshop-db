package com.ase.app.spring.service;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.ase.app.spring.model.Client;
import com.ase.app.spring.model.Ticket;
import com.ase.app.spring.validation.PriceValidator;

@Service
public class TicketServiceImpl implements TicketService {
	
	@Value("${app.ticket.baseprice}")
	private int basePrice;
	
	@Autowired
	private PriceValidator priceValidator;
	
	public Ticket reserve(Client client, Date flightDate) {
		Ticket ticket = new Ticket();
		ticket.setClient(client);
		ticket.setFlightDate(flightDate);
		ticket.setPrice(getPrice(flightDate));
		return ticket;
	}
	
	private BigDecimal getPrice(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		
		int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
		
		return BigDecimal.valueOf(dayOfWeek + basePrice);
	}

	public Collection<BigDecimal> getPrices(Date startDate, Date endDate) {
		// TODO Auto-generated method stub
		priceValidator.validateStartEndDate(startDate, endDate);
		
		
		return null;
	}

}
