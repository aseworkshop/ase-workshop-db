package com.ase.app.spring.validation;

import java.util.Date;

import org.springframework.stereotype.Component;

@Component
public class PriceValidator {
	public void validateStartEndDate(Date startDate, Date endDate) 
			throws ValidationException {
		if (startDate == null || endDate == null) {
			throw new ValidationException("Start and End date cannot be null");
		}
		if (startDate.compareTo(endDate) > 0) {
			throw new ValidationException("Start date should be smaller than End date");
		}
	}
}
