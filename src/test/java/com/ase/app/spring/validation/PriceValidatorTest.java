package com.ase.app.spring.validation;

import java.util.Date;

import org.junit.BeforeClass;
import org.junit.Test;

import com.ase.app.spring.validation.PriceValidator;
import com.ase.app.spring.validation.ValidationException;

public class PriceValidatorTest {
	
	private static PriceValidator priceValidator;
	
	@BeforeClass
	public static void init()
	{
		priceValidator = new PriceValidator();
	}
	
	@Test(expected=ValidationException.class)
	public void testNullStartDate() {
		priceValidator.validateStartEndDate(null, new Date());
	}
	
	@Test(expected=ValidationException.class)
	public void testNullEndDate(){
		priceValidator.validateStartEndDate(new Date(), null);
	}
	
	@Test(expected=ValidationException.class)
	public void testInvalidEndDate()
		throws InterruptedException
	{
		Date endDate = new Date(System.currentTimeMillis());
		Thread.sleep(1);
		Date startDate = new Date(System.currentTimeMillis());
		
		//startdate is now later than end date
		priceValidator.validateStartEndDate(startDate, endDate);
	}
	
	@Test
	public void testValidEndDate()
		throws InterruptedException
	{
		Date startDate = new Date(System.currentTimeMillis());
		Thread.sleep(1);
		Date endDate = new Date(System.currentTimeMillis());
		
		//end date is now later than start date
		priceValidator.validateStartEndDate(startDate, endDate);
	}
	
}
